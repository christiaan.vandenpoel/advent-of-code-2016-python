from aoc.day02 import part1, part2

example_input = """ULL
RRDDD
LURDL
UUUUD""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 1985

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == '5DB3'
