from aoc.day01 import part1, part2

example_input = """"""
#
# --- Part One ---
#

def test_part1():
    assert part1.result(["R2, L3"]) == 5
    assert part1.result(["R2, R2, R2"]) == 2
    assert part1.result(["R5, L5, R5, R3"]) == 12

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(["R8, R4, R4, R8"]) == 4
