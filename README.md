This repository is a collection of my solutions for the [Advent of Code 2021](http://adventofcode.com/2021) calendar.

Puzzle                                                    |
--------------------------------------------------------- |
[Day 1: No Time for a Taxicab](aoc/day01)                 |
[Day 2: Bathroom Security](aoc/day02)                     |
[Day 3: Squares With Three Sides](aoc/day03)              |
[Day 4: ](aoc/day04)                                      |
[Day 5: ](aoc/day05)                                      |
[Day 6: ](aoc/day06)                                      |
[Day 7: ](aoc/day07)                                      |   
[Day 8: ](aoc/day08)                                      |
[Day 9: ](aoc/day09)                                      |
[Day 10: ](aoc/day10)                                     |
[Day 11: ](aoc/day11)                                     |
[Day 12: ](aoc/day12)                                     |
[Day 13: ](aoc/day13)                                     |
[Day 14: ](aoc/day14)                                     |
[Day 15: ](aoc/day15)                                     |
[Day 16: ](aoc/day16)                                     |
[Day 17: ](aoc/day17)                                     |
[Day 18: ](aoc/day18)                                     |
[Day 19: ](aoc/day19)                                     |
[Day 20: ](aoc/day20)                                     |
[Day 21: ](aoc/day21)                                     |
[Day 22: ](aoc/day22)                                     |
[Day 23: ](aoc/day23)                                     |
[Day 24: ](aoc/day24)                                     |
[Day 25: ](aoc/day25)                                     |
