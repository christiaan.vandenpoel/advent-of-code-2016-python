# Advent of Code - Day 1 - Part One
from .util import movement_map

def result(input):
    movements = input[0].split(", ")
    (x,y) = (0,0)
    direction = 'north'
    for movement in movements:
        direction = movement_map[direction][movement[0]]

        steps = int(movement[1:])

        if direction == 'north':
            y += steps
        elif direction == 'east':
            x += steps
        elif direction == 'south':
            y -= steps
        elif direction == 'west':
            x -= steps
        
    return abs(x) + abs(y)
