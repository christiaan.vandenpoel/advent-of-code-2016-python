movement_map = {
    'north': {
        'L': 'west',
        'R': 'east'
    },
    'east': {
        'L': 'north',
        'R': 'south'
    },
    'south': {
        'L': 'east',
        'R': 'west'
    },
    'west': {
        'L': 'south',
        'R': 'north'
    },
}