# Advent of Code - Day 1 - Part Two
from pprint import pprint
from .util import movement_map

def result(input):
    movements = input[0].split(", ")
    (x,y) = (0,0)
    visited = set()
    visited.add((x,y))
    direction = 'north'

    for movement in movements:
        direction = movement_map[direction][movement[0]]

        steps = int(movement[1:])

        if direction == 'north':
            extra_moves = [(x,y+step) for step in range(1,steps+1)]
            y += steps
        elif direction == 'east':
            extra_moves = [(x+step,y) for step in range(1,steps+1)]
            x += steps
        elif direction == 'south':
            extra_moves = [(x,y-step) for step in range(1,steps+1)]
            y -= steps
        elif direction == 'west':
            extra_moves = [(x-step,y) for step in range(1,steps+1)]
            x -= steps
        
        for (x,y) in extra_moves:
            if (x,y) in visited:
                return abs(x) + abs(y)
            else:
                visited.add((x,y))
        
    return 0

