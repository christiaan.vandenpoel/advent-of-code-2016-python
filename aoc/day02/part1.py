# Advent of Code - Day 2 - Part One
from pprint import pprint

# 1 2 3
# 4 5 6
# 7 8 9

movement_map = {
    1: { 'U': 1, 'L': 1, 'D': 4, 'R': 2 },
    2: { 'U': 2, 'L': 1, 'D': 5, 'R': 3 },
    3: { 'U': 1, 'L': 2, 'D': 6, 'R': 3 },
    4: { 'U': 1, 'L': 4, 'D': 7, 'R': 5 },
    5: { 'U': 2, 'L': 4, 'D': 8, 'R': 6 },
    6: { 'U': 3, 'L': 5, 'D': 9, 'R': 6 },
    7: { 'U': 4, 'L': 7, 'D': 7, 'R': 8 },
    8: { 'U': 5, 'L': 7, 'D': 8, 'R': 9 },
    9: { 'U': 6, 'L': 8, 'D': 9, 'R': 9 }
}

def result(input):
    output = 0
    next_pos = 5
    for movements in input:
        for movement in movements:
            next_pos = movement_map[next_pos][movement]
        output = output * 10 + next_pos

    return output
